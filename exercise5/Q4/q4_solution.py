def climb_combinations(n):
    if n < 1:
        return 0

    if n == 1:
        return 1

    if n == 2:
        return 2

    return climb_combinations(n -1) + climb_combinations(n - 2)

# test
print(climb_combinations(0))
print(climb_combinations(1))
print(climb_combinations(2))
print(climb_combinations(3))
print(climb_combinations(10))

def is_valid_paren(s, cnt = 0):
    if len(s) == 0:
        if cnt == 0:
            return True
        else:
            return False

    # get next next character
    # if ( count++
    # if ) count-- , if cnt < 0 return false
    # other char, don't change the count
    # call the recursive method with the rest of the string

    next_char = s[0]
    remaining_string = s[1:]

    if next_char == "(":
        cnt += 1

    if next_char == ")":
        cnt -= 1
        if cnt < 0:
            return False

    return is_valid_paren(remaining_string, cnt)




# test
print("false:", is_valid_paren(")"))
print("false:", is_valid_paren("("))
print("true:", is_valid_paren("()"))
print("false:", is_valid_paren(") ("))
print("false:", is_valid_paren("())()"))
print("false:", is_valid_paren("(.(a)"))
print("true:", is_valid_paren("p(()r((0)))"))
print("true:", is_valid_paren("(())"))

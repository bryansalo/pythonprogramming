

def reverse_string(input_string):

    # base case
    if len(input_string) <= 1:
        return input_string

    # take the first character, remove it
    # return chopped string + first character
    first_charecter = input_string[0]
    chopped_string = input_string[1:]
    return reverse_string(chopped_string) + first_charecter



# test code
print(reverse_string("abc"))
print(reverse_string("Hello!"))
print(reverse_string(""))
